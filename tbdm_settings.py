import sublime
from .tbdm_utils import *

PLACEHOLDER_SECRET = "replaceSecret"

class Settings(object):
	def __init__(self):
		self._sub_settings = sublime.load_settings('TBDmanagement.sublime-settings')
		self._file_settings = sublime.load_settings('tbdm.sublime-settings')
		self._overrides = {}

	def get(self, key, default=None):
		if key in self._overrides:
			return self._overrides[key]
		else:
			return self._sub_settings.get(key, default)

	def getUserSetting(self, key, default=None):
		return self._sub_settings.get(key, default)

	def set(self, key, value):
		self._overrides[key] = value
	
	def setUserSetting(self, key, value):
		self._sub_settings.set(key, value)

	def has(self, key):
		found = key in self._overrides
		if not found:
			found = self._sub_settings.has(key)
		return found

	def hasUserSetting(self, key):
		return self._sub_settings.has(key)

	def saveUserSettings(self):
		sublime.save_settings('TBDmanagement.sublime-settings')

	def saveColorScheme(self, colorSchemePath):
		self._file_settings.set('color_scheme', colorSchemePath)
		sublime.save_settings('tbdm.sublime-settings')

	def openPasswordSafe(self):
		pathToSafe = sublime.packages_path() + "/User/password.safe"
		safeView = sublime.active_window().open_file(pathToSafe)
		self.selectSecretInOpenFile(safeView)

	def selectSecretInOpenFile(self, safeView):
		if safeView.is_loading():
			sublime.set_timeout(lambda: self.selectSecretInOpenFile(safeView), 10)
		else:
			safeView.sel().clear()
			regions = safeView.find_all(PLACEHOLDER_SECRET)
			log(regions)
			safeView.sel().add_all(regions)
		
		
	def setKeyInSafe(self, key):
		passwordSafe = sublime.load_settings('password.safe')
		passwordSafe.set(key, PLACEHOLDER_SECRET)
		sublime.save_settings('password.safe')

	def getPasswordInSafe(self, key):
		passwordSafe = sublime.load_settings('password.safe')
		return passwordSafe.get(key)


