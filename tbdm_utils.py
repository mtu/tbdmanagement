import sublime, sublime_plugin
import inspect, logging
import re
from .constants import C

class TBDException(Exception):
	def __init__(self, value, showError=True):
		self.value = value
		self.showError = showError

	def __str__(self):
		return repr(self.value)

def log(*args):
	# Get the previous frame in the stack, otherwise it would
	# be this function!!!
	frame = inspect.currentframe().f_back
	frameInfo = inspect.getframeinfo(frame)
	filename = frameInfo.filename.split("/")[-1]
	# Dump the message + the name of this function to the log.
	print("[%s:%i#%s] %s" % (
		filename, 
		frameInfo.lineno,
		frameInfo.function, 
		args
	))

def printTime(time, timeOutputFormat=C.S_TIME_FORMAT_DECIMAL):
	if C.S_TIME_FORMAT_JIRA == timeOutputFormat:
		negativeTime = False
		negativeSign = ""
		time = float(time)
		if time < 0:
			negativeTime = True
			time = abs(time)
		weeks, remainder = divmod(time, 40)
		days, remainder = divmod(remainder, 8)
		hours, remainder = divmod(remainder, 1)
		minutes = remainder * 60
		if negativeTime:
			negativeSign = "-"
		return '%s%sw %sd %sh %sm' % (negativeSign, int(weeks), int(days), int(hours), int(minutes))
	else:
		return str(time) + "h"

def parseTime(time, timeInputFormat=C.S_TIME_FORMAT_DECIMAL):
	if not time:
		return float(0)
	if C.S_TIME_FORMAT_JIRA == timeInputFormat:
		# log("time", time)
		stripped = "".join(time.split())
		# log("stripped", stripped)
		pattern = re.compile("((?P<w>\d*)w)?((?P<d>\d*)d)?((?P<h>\d*)h)?((?P<m>\d*)m)?")
		match = pattern.match(stripped)
		if not match:
			raise TBDException("Time not parsable")
		splitted = match.groupdict()
		# log(splitted)
		parsedTime = 0
		if splitted.get("w"):
			parsedTime += 40*int(splitted.get("w"))
		if splitted.get("d"):
			parsedTime += 8*int(splitted.get("d"))
		if splitted.get("h"):
			parsedTime += int(splitted.get("h"))
		if splitted.get("m"):
			parsedTime += int(splitted.get("m")) / 60
		return parsedTime
	else:
		return float(time)

