import sublime, sublime_plugin
from .constants import C
from .tbdm_settings import Settings
from .tbdm_utils import *

C_KEY = "key"
C_VALUE = "value"

colorSchemes = [
	{C_KEY: "Dark (Monokai plus)", C_VALUE: "Packages/tbdmanagement/TDBM-Dark(Monokai).tmTheme"},
	{C_KEY: "Light (Eiffel plus)", C_VALUE: "Packages/tbdmanagement/TBDM-Light(Eiffel).tmTheme"}
]

class StyleTbdCommand(sublime_plugin.WindowCommand):

	def run(self):
		isDevEnv = Settings().get(C.S_IS_DEV_ENV, False)
		if (isDevEnv):
			print("#### Starting StyleTBD ####")
		try:
			self.settings = Settings()
			items = [entry[C_KEY] for entry in colorSchemes]
			self.window.show_quick_panel(items, self.changeColorScheme, False, -1)

		except TBDException as e:
			print("TBDException: " + e.value)
			if e.showError:
				sublime.error_message(e.value)
		except:
			e = sys.exc_info()[0]
			sublime.error_message("Unexpected Error, see log for more.\n" + str(e))
			raise e
		
		if (isDevEnv):
			print("#### Finished StyleTBD ####")


	def changeColorScheme(self, index):
		if index < 0:
			return
		selectedColorScheme = colorSchemes[index]
		self.settings.saveColorScheme(selectedColorScheme[C_VALUE])
