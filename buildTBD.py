import sublime, sublime_plugin
import inspect
import operator
import re
import sys
from .constants import C
from .tbdm_settings import Settings
from .tbdm_utils import *


class Task:
	def __init__(self):
		self.attributes = {}

	def __str__(self):
		return repr(self.attributes)

	def __repr__(self):
		return repr(self.attributes)

class Story:
	key = ""
	tasks = []
	sums = {}
	rank = -1

	def __init__(self, value, rank):
		self.key = value
		self.tasks = []
		self.sums = {}
		self.rank = rank

	def __str__(self):
		return self.key + ": " + repr(self.sums) + "\n"

	def __repr__(self):
		return self.key + ": " + repr(self.tasks)


class BuildTbdCommand(sublime_plugin.TextCommand):

	linePattern = [
		{"type" : C.T_EMTPY, "pattern" : re.compile("^\s*$")},
		{"type" : C.T_OUTPUT, "pattern" : re.compile("^>[^>]+")},
		{"type" : C.T_COMMAND, "pattern" : re.compile("^>>\s?(?P<command>\w+)(\((?P<query>.*)\))?$")},
		{"type" : C.T_VARIABLE, "pattern" : re.compile("^\$(?P<var>\w+)=(?P<value>\w*)")},
		{"type" : C.T_DEFINITION, "pattern" : re.compile("^§(?P<def>\w+)(\[(?P<field>\w+)\])?=(?P<value>(\w|-)*(,(\w|-)+)*)")},
		{"type" : C.T_HEAD, "pattern" : re.compile("^(?P<method>HEADER|INSERT|UPDATE) .*")},
		{"type" : C.T_TASK, "pattern" : re.compile(".*;.*")},
		{"type" : C.T_COMMENT, "pattern" : re.compile("^#.*")}
	]

	header = {}
	definitions = {}
	stories = {}
	settings = {}


	# Functions
	def parseLine(self, line):
		for entry in self.linePattern:
			match = entry["pattern"].match(line)
			if match:
				return entry["type"], match.groupdict()
		return "IGNORE", {}

	def parseSumAttribute(self, attribute):
		timeInputFormat = self.settings.get(C.S_TIME_FORMAT_INPUT)
		return parseTime(attribute, timeInputFormat)

	def printTime(self, time):
		timeOutputFormat = self.settings.get(C.S_TIME_FORMAT_OUTPUT)
		return printTime(time, timeOutputFormat)


	# Outputs
	def timeOutput(self, sums, maxIncluding=False):
		tmplMax = "\n> {0}: {1} - {2} = {3};"
		tmpl = "{0}: {1}; "
		retVal = ""
		totalTime = 0
		sorted_sums = sorted(sums.items(), key=operator.itemgetter(0))
		for key in self.sums:
			if key in sums:
				value = sums[key]
				totalTime += value
				if maxIncluding and self.definitions.get(C.D_MAX).get(key):
					maxValue = self.definitions.get(C.D_MAX).get(key)
					left = int(maxValue) - value
					retVal += tmplMax.format(key, self.printTime(maxValue), self.printTime(value), self.printTime(left))
				else:
					retVal += tmpl.format(key, str(self.printTime(value)))

		if sums.values() and len(sums.values()) > 1:
			if maxIncluding:
				retVal += "\n> "
			retVal += "combined: " + self.printTime(totalTime)
		return retVal

	def sumOutput(self, story):
		tmpl = "> {0}: {1}\n"
		time = self.timeOutput(story.sums)
		return tmpl.format(str(story.key), time)

	def totalOutput(self, stories):
		tmpl = "> total: {0}\n"
		sums = {}
		for story in stories.values():
			for key, value in story.sums.items():
				attrSum = sums.get(key, 0) + value
				sums.update({key : attrSum})
		return tmpl.format(self.timeOutput(sums, True))

	def commentLineRegion(self, edit, lineRegion):
		self.view.insert(edit, lineRegion.begin(), "# ")
		selection = self.view.sel()
		addedRegion = sublime.Region(lineRegion.begin(), lineRegion.begin()+2)
		selection.clear()
		selection.add(addedRegion)
	
	def duplicateHeader(self, edit, lineRegion):
		try:
			if not self.headerLine or not self.settings.get(C.S_DUPLICATE_HEADER, False):
				return
			self.view.insert(edit, lineRegion.end(), "> " + self.headerLine)
		except AttributeError:
			return

	# handle functions
	def handleCOMMAND(self, edit, lineRegion, args):
		command = args[C.G_COMMAND]
		commentLineRegion = False

		if C.C_JQL == command:
			if args[C.G_QUERY]:
				if not self.settings.has(C.S_SELECTED_JIRA_CONFIG):
					jiraConfigId = self.view.window().run_command(C.R_JIRA_TBD, 
						{
							"operation": C.O_SELECT_JIRA_CONFIG, 
							"args": {"cursorIndex": lineRegion.begin()}
						})
					raise TBDException("ReRun Command", False)

				text = self.view.window().run_command(C.R_JIRA_TBD, 
					{
						"operation": C.O_QUERY_JIRA, 
						"args": {
							"query": args[C.G_QUERY],
							"jiraConfigId": self.settings.get(C.S_SELECTED_JIRA_CONFIG),
							"cursorIndex": lineRegion.end(),
							"fieldIds": ",".join(self.header.values())
						}
					})
				commentLineRegion = True
			else:
				raise TBDException("jql command needs a query.")
		elif C.C_SUM == command:
			text = self.sumOutput(self.stories.get(self.definitions[C.D_PARENT]))
		elif C.C_TOTAL == args[C.G_COMMAND]:
			text = self.totalOutput(self.stories)
		elif C.C_SUMMARY == args[C.G_COMMAND]:
			text = ""
			storedStories = sorted(self.stories.values(), key=lambda story: story.rank)
			for story in storedStories:
				text += self.sumOutput(story)
			text += self.totalOutput(self.stories)
		else:
			text = None

		if text:
			self.view.insert(edit, lineRegion.end(), text)
		if commentLineRegion:
			self.commentLineRegion(edit, lineRegion)


	def handleVARIABLE(self, edit, lineRegion, args):
		pass

	def handleDEFINITION(self, edit, lineRegion, args):
		value = args[C.G_VALUE]
		definition = args[C.G_VALUE]
		if value:
			if "," in value:
				value = args[C.G_VALUE].split(",")
			if args[C.G_DEFINITION] == C.D_PARENT:
				story = Story(args[C.G_VALUE], len(self.stories))
				self.stories.update({args[C.G_VALUE] : story})
				
				self.duplicateHeader(edit, lineRegion)
			elif args[C.G_DEFINITION] == C.D_SUM:
				self.sums = value
			elif args[C.G_DEFINITION] == C.D_SETTING:
				if not args.get(C.G_FIELD):
					raise TBDException("Settings attribute missing.")
				else:
					if "true" == value.lower():
						value = True
						self.settings.setUserSetting(args[C.G_FIELD], value)
					elif "false" == value.lower():
						value = False
						self.settings.setUserSetting(args[C.G_FIELD], value)
					self.settings.set(args[C.G_FIELD], value)

			
			if args.get(C.G_FIELD):
				maxDict = self.definitions.get(C.D_MAX, dict({}))
				maxDict.update({args[C.G_FIELD] : value})
				self.definitions.update({args[C.G_DEFINITION] : maxDict})
			else:
				self.definitions.update({args[C.G_DEFINITION] : value})
			#self.definitions.update(dictionary)

				#print(self.stories)
			#lprint(self.definitions)
				


	def handleHEADER(self, edit, lineRegion, args):
		line = self.view.substr(lineRegion)
		attributesLine = line[len(args[C.G_METHOD])+1:len(line)]
		attributes = attributesLine.split(";")
		attributes = [x.strip() for x in attributes]
		if len(attributes) > 0:
			self.header.clear()
			self.headerLine = line
			for index, attribute in enumerate(attributes):
				key = index
				self.header.update({key : attribute})
			#print(self.header)


	def handleTASK(self, edit, lineRegion, args):
		if not self.header:
			raise TBDException("Header is missing.")
		elif not self.definitions.get(C.D_PARENT):
			raise TBDException("Parent not set.")
		else:
			story = self.stories[self.definitions.get(C.D_PARENT)]
			task = Task()
			story.tasks.append(task)
			line = self.view.substr(lineRegion)
			attributes = [x.strip() for x in line.split(";")]
			if len(attributes) > 0:
				for index, attribute in enumerate(attributes):
					task.attributes.update({self.header[index] : attribute})
					if self.header[index] in self.definitions[C.D_SUM]:
						updatedSum = story.sums.get(self.header[index], 0) + self.parseSumAttribute(attribute)
						story.sums.update({self.header[index] : updatedSum})
			#self.view.insert(edit, lineRegion.begin(), "| TASK | ")




	def callFunctionByType(self, type, edit, lineRegion, args={}):
		if type == C.T_COMMAND:
			return self.handleCOMMAND(edit, lineRegion, args)
		elif type == C.T_VARIABLE:
			return self.handleVARIABLE(edit, lineRegion, args)
		elif type == C.T_DEFINITION:
			return self.handleDEFINITION(edit, lineRegion, args)
		elif type == C.T_HEAD:
			return self.handleHEADER(edit, lineRegion, args)
		elif type == C.T_TASK:
			return self.handleTASK(edit, lineRegion, args)


	def run(self, edit):
		isDevEnv = Settings().get(C.S_IS_DEV_ENV, False)
		if (isDevEnv):
			print("#### Starting BuildTBD ####")

		self.stories.clear()
		self.header.clear()
		self.definitions.clear()
		self.settings = Settings()
		
		try:
			self.view.run_command(C.R_CLEAN_TBD)

			cursor = 0
			while (cursor < self.view.size()):
				lineRegion = self.view.full_line(cursor)
				key, args = self.parseLine(self.view.substr(lineRegion))
				self.callFunctionByType(key, edit, lineRegion, args)
				
				lineRegion = self.view.full_line(cursor)
				cursor += lineRegion.size()
		except TBDException as e:
			print("Error: " + e.value)
			if e.showError:
				sublime.error_message(e.value)
		except:
			e = sys.exc_info()[0]
			sublime.error_message("Unexpected Error, see log for more.\n" + str(e))
			raise e

		if (isDevEnv):
			print("#### Finished BuildTBD ####")
