import sublime, sublime_plugin
import inspect
import sys
from jira.client import JIRA
from jira.exceptions import JIRAError
from .constants import C
from .buildTBD import TBDException
from .tbdm_settings import Settings
from .tbdm_utils import *

_jiraConnection = None
_jiraConfigId = None

def lprint(*args):
	print(inspect.currentframe().f_back.f_lineno, args)

class JiraConfig:
	configId = None
	server = None
	login = None
	password = None
	project = None

	def __init__(self, configDict):
		self.configId = configDict.get(C.S_JIRA_CONFIG_ID)
		self.server = configDict.get(C.S_JIRA_SERVER)
		self.login = configDict.get(C.S_JIRA_LOGIN)
		self.password = configDict.get(C.S_JIRA_PASSWORD)
		self.project = configDict.get(C.S_JIRA_PROJECT)


class JiraTbdCommand(sublime_plugin.WindowCommand):

	def run(self, operation, args={}):
		isDevEnv = Settings().get(C.S_IS_DEV_ENV, False)
		if (isDevEnv):
			print("#### Starting JiraTBD ####")
		try:
			#raise TBDException("a test")
			self.settings = Settings()
			self.loadJiraConfigs()

			query = args.get("query", None)
			jiraConfigId = args.get("jiraConfigId", None)
			cursorIndex = args.get("cursorIndex", None)
			fieldIds = args.get("fieldIds", "").split(",")

			if C.O_SELECT_JIRA_CONFIG == operation:
				if not cursorIndex:
					cursorIndex = self.window.active_view().sel()[0].begin()
				self.tempArgs = {"cursorIndex": cursorIndex}
				self.selectJiraConfig(self.setJiraConfigByIndexAndInsertSetting)
			elif C.O_QUERY_JIRA == operation:
				return self.query(query, jiraConfigId, cursorIndex, fieldIds)


		except JIRAError as e:
			sublime.error_message(str(e))
		except TBDException as e:
			print("TBDException: " + e.value)
			if e.showError:
				sublime.error_message(e.value)
		except:
			e = sys.exc_info()[0]
			sublime.error_message("Unexpected Error, see log for more.\n" + str(e))
			raise e
		
		if (isDevEnv):
			print("#### Finished JiraTBD ####")

	def loadJiraConfigs(self):
		self.jiraConfigs = [JiraConfig(config) for config in self.settings.get(C.S_JIRA_CONFIGS)]

	def selectJiraConfig(self, callback):
		if not callback:
			callback = self.setJiraConfigByIndex

		self.jiraConfigs = [JiraConfig(config) for config in self.settings.get(C.S_JIRA_CONFIGS)]
		items = [self.getJiraConfigInfo(config) for config in self.jiraConfigs]
		items.append(["Create a new Config", 
			"Collects the data through the input panel and saves the new config."])
		self.window.show_quick_panel(items, callback, False, -1)

	def getJiraConfigInfo(self, jiraConfig):
		title = jiraConfig.configId
		subTitle = jiraConfig.project + " @ " + jiraConfig.server

		return [title, subTitle]

	def setJiraConfigByIndex(self, configIndex):
		if configIndex > -1:
			if configIndex >= len(self.jiraConfigs):
				self.window.run_command(C.R_JIRA_CONFIG_TBD)
			else:
				return self.setJiraConfig(self.jiraConfigs[configIndex])

	def setJiraConfigById(self, configId):
		for config in self.jiraConfigs:
			if config.configId == configId:
				self.setJiraConfig(config)
				return
		raise TBDException("No config with id '{0}' found".format(configId))

	def setJiraConfig(self, jiraConfig):
		global _jiraConnection
		global _jiraConfigId

		if not _jiraConfigId or _jiraConfigId != jiraConfig.configId:
			configId, jiraConnection = self.createJiraConnection(jiraConfig)
			self.jiraConnection = jiraConnection
			_jiraConnection = jiraConnection
			_jiraConfigId = configId
		else:
			self.jiraConnection = _jiraConnection
		return _jiraConfigId

	def createJiraConnection(self, jiraConfig):
		options = {'server': jiraConfig.server}
		password = Settings().getPasswordInSafe(jiraConfig.configId)
		if not password:
			sublime.error_message("Password missing for: %s" % jiraConfig.configId)
			return (None, None)
		jira = JIRA(options, basic_auth=(jiraConfig.login, password))
		return (jiraConfig.configId, jira)

	def setJiraConfigByIndexAndInsertSetting(self, configIndex):
		jiraConfigId = self.setJiraConfigByIndex(configIndex)
		if jiraConfigId and self.tempArgs.get("cursorIndex"):
			configText = "§{0}[{1}]={2}\n".format(C.D_SETTING, C.S_SELECTED_JIRA_CONFIG, jiraConfigId)
			self.window.active_view().run_command(C.R_INSERT_TEXT_TBD,
				{"insertAt": self.tempArgs.get("cursorIndex"),
				"text": configText})
	
	def outputSubtask(self, subtask, fieldIds=[]):
		# if subtask.fields.timeestimate:
		# 	timeestimate = int(subtask.fields.timeestimate) / 3600
		# else:
		# 	timeestimate = ""
		output = "> # {0} / {1}\n".format(subtask.key, subtask.fields.summary)
		# if fieldIds:
			# output += "> # "
			# for fieldId in fieldIds:
			# 	output += subtask.fields.get(fieldId) + ";"
			# output += "\n"
		return output

	def outputResult(self, result, fieldIds=[]):
		output = "> Remove '> ' in the lines you want to keep.\n"
		for issue in result:
			if issue.fields.aggregatetimeestimate:
				aggregateTimeEstimate = int(issue.fields.aggregatetimeestimate) / 3600
			else:
				aggregateTimeEstimate = 0
			aggregateTimeEstimateJira = printTime(aggregateTimeEstimate, C.S_TIME_FORMAT_JIRA)
			output += "> §parent={0}\n> # {1} // {2} / {3}\n".format(issue.key, 
				issue.fields.summary, printTime(aggregateTimeEstimate), aggregateTimeEstimateJira)
			if self.settings.get(C.S_FETCH_JIRA_SUBTASKS, False) and issue.fields.subtasks:
				for subtask in issue.fields.subtasks:
					output += self.outputSubtask(subtask, fieldIds)
			output += "> \n"
		return output


	def query(self, query, jiraConfigId=None, cursorIndex=None, fieldIds=[]):

		if not query:
			raise TBDException("Query needed.")


		if not jiraConfigId:
			raise TBDException("JiraConfigId needed.")
		else:
			self.setJiraConfigById(jiraConfigId)

		fieldsToQuery = "summary,timeestimate,aggregatetimeestimate"
		if self.settings.get(C.S_FETCH_JIRA_SUBTASKS, False):
			fieldsToQuery += ",subtasks"

		result = self.jiraConnection.search_issues(query, maxResults=25, fields=fieldsToQuery)
		text = self.outputResult(result, fieldIds)
		self.window.active_view().run_command(C.R_INSERT_TEXT_TBD,
			{"insertAt": cursorIndex, "text": text})


class JiraConfigTbdCommand(sublime_plugin.WindowCommand):
	currentConfig = None

	def run(self):
		self.showInputPanel("Config-Id:", "", self.askForServer)

	def showInputPanel(self, caption, text, onDone):
		self.window.show_input_panel(caption, text, onDone, None, self.onCancel)


	def onCancel(self):
		self.currentConfig = None

	def askForServer(self, configId):
		configId = configId.replace(" ", "_")
		self.currentConfig = {C.S_JIRA_CONFIG_ID: configId}
		self.showInputPanel("ServerAddress:", "https://", self.askForProject)

	def askForProject(self, server):
		if self.currentConfig:
			if not server.startswith("https://"):
				self.showInputPanel("ServerAddress:", "https://", self.askForLogin)
				return

			self.currentConfig.update({C.S_JIRA_SERVER : server})
			self.showInputPanel("Project:", "", self.askForLogin)
		

	def askForLogin(self, project):
		if self.currentConfig:
			self.currentConfig.update({C.S_JIRA_PROJECT : project})
			self.showInputPanel("Username:", "", self.saveConfig)

	def saveConfig(self, login):
		if not self.currentConfig:
			return

		self.currentConfig.update({C.S_JIRA_LOGIN : login})
		self.settings = Settings()

		configs = self.settings.getUserSetting(C.S_JIRA_CONFIGS)
		
		newConfigId = self.currentConfig.get(C.S_JIRA_CONFIG_ID)
		for config in configs:
			if config.get(C.S_JIRA_CONFIG_ID) == newConfigId:
				configs.remove(config)

		configs.append(self.currentConfig)
		

		self.settings.setUserSetting(C.S_JIRA_CONFIGS, configs)
		self.settings.saveUserSettings()
		
		self.settings.setKeyInSafe(self.currentConfig.get(C.S_JIRA_CONFIG_ID))

		approved = sublime.ok_cancel_dialog("Open PasswordSafe?")
		if approved:
			self.settings.openPasswordSafe()

