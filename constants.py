class C:
	# Types
	T_COMMAND = "COMMAND"
	T_COMMENT = "COMMENT"
	T_DEFINITION = "DEFINITION"
	T_EMTPY = "EMTPY"
	T_HEAD = "HEAD"
	T_OUTPUT = "OUTPUT"
	T_TASK = "TASK"
	T_VARIABLE = "VARIABLE"

	# PatternGroupNames
	G_COMMAND = "command"
	G_DEFINITION = "def"
	G_FIELD = "field"
	G_METHOD = "method"
	G_QUERY = "query"
	G_VALUE = "value"
	G_VARIABLE = "var"

	# Commands
	C_JQL = "jql"
	C_SUM = "sum"
	C_SUMMARY = "summary"
	C_TOTAL = "total"

	# Definitions
	D_ATTRIBUTE = "attribute"
	D_MAX = "max"
	D_PARENT = "parent"
	D_SUM = "sum"
	D_SETTING = "setting"

	# Settings
	S_FILENAME = "TBDmanagement.sublime-settings"
	S_DECIMAL_SEPARATOR = "decimal_separator"
	S_LIST_SEPARATOR = "list_separator"
	S_TIME_FORMAT_INPUT = "time_format_input"
	S_TIME_FORMAT_OUTPUT = "time_format_output"
	S_TIME_FORMAT_DECIMAL = "decimal"
	S_TIME_FORMAT_JIRA = "jira"
	S_JIRA_CONFIGS = "jira_configs"
	S_JIRA_CONFIG_ID = "jira_config_id"
	S_JIRA_SERVER = "jira_server"
	S_JIRA_LOGIN = "jira_login"
	S_JIRA_PASSWORD = "jira_password"
	S_JIRA_PROJECT = "jira_project"
	S_SELECTED_JIRA_CONFIG = "selected_jira_config"
	S_FETCH_JIRA_SUBTASKS = "fetch_jira_subtasks"
	S_IS_DEV_ENV = "is_dev_env"
	S_DUPLICATE_HEADER = "duplicate_header"

	# Operations
	O_QUERY_JIRA = "query_jira"
	O_CREATE_JIRA_CONFIG = "create_jira_config"
	O_SELECT_JIRA_CONFIG = "select_jira_config"

	# Run_Commands
	R_CLEAN_TBD = "clean_tbd"
	R_JIRA_TBD = "jira_tbd"
	R_JIRA_CONFIG_TBD = "jira_config_tbd"
	R_INSERT_TEXT_TBD = "insert_text_tbd"