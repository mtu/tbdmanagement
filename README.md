# TBDmanagement #

This is a Plugin to manage your TaskBreakDown in Sublime Text.
Use the comfort of writing tasks in a TextEditor and have the advantage of time-calculation, Syntax-Highlighting and Jira-Support.

## Installation ##
TBDmamangement is implemented for Sublime Text 3. No promises done, that it works with Sublime Text 2!

**OS X:**
```
#!text
git clone https://bitbucket.org/mtu/tbdmanagement.git ~/Library/Application\ Support/Sublime\ Text\ 3/Packages/TDBmanagement
```
**Windows:**
```
#!text
git clone https://bitbucket.org/mtu/tbdmanagement.git "%APPDATA%/Sublime Text 3/Packages/TDBmanagement"
```

## Usage ##
Use `cmd+alt+t` (OS X) or `ctrl+alt+t` (Win) to pull up the command palette with all available commands.

### Available commands ###
* Build
* Clean Output
* Insert Setting
* Select Jira Config
* Create Jira Config

### Example ###
```
#!tbdm
# This is a Comment

§sum=orig,est
§max[orig]=20
§max[est]=30

§parent=TP-1
HEADER orig;est;summary
5;2;First Task
4;4.5;Second Task

>> sum
>> total

§setting[time_format_output]=jira
>> summary

>> jql(issuetype = Story)
```

### Syntax Highlighting ###
Use the file-extension `.tbdm` or active syntax `TBDM`.