import sublime, sublime_plugin
import inspect
import re
from .tbdm_settings import Settings


def lprint(*args):
	print(inspect.currentframe().f_back.f_lineno, args)

class CleanTbdCommand(sublime_plugin.TextCommand):

	def deleteOutputLine(self, edit):
		region = self.view.find("^>[^>][^\n]*$", 0)
		if (region):
			self.view.erase(edit, self.view.full_line(region))
			return True
		else:
			return False

	def addEmptyLineIfNecessary(self, edit):
		lastLine = self.view.substr(self.view.full_line(self.view.size()))
		emptyLinePattern = re.compile("^\s*$")
		match = emptyLinePattern.match(lastLine)
		if not match:
			self.view.insert(edit, self.view.size(),"\n")
		
	def run(self, edit):
		print("#### Starting CleanTBD ####")

		self.addEmptyLineIfNecessary(edit)

		while (self.deleteOutputLine(edit)):
			continue

		print("#### Finished CleanTBD ####")
