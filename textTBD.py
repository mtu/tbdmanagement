import sublime, sublime_plugin
import inspect
import re
import sys
from .constants import C
from .tbdm_settings import Settings
from .tbdm_utils import *


C_KEY = "key"
C_VALUES = "values"

settingsMapping = [
	{C_KEY: C.S_TIME_FORMAT_OUTPUT, C_VALUES: [C.S_TIME_FORMAT_DECIMAL, C.S_TIME_FORMAT_JIRA]},
	{C_KEY: C.S_TIME_FORMAT_INPUT, C_VALUES: [C.S_TIME_FORMAT_DECIMAL, C.S_TIME_FORMAT_JIRA]},
	{C_KEY: C.S_FETCH_JIRA_SUBTASKS, C_VALUES: ["true", "false"]},
	{C_KEY: C.S_DUPLICATE_HEADER, C_VALUES: ["true", "false"]}

]

class TextTbdCommand(sublime_plugin.TextCommand):

	def insertSetting(self, index):
		if index > -1:
			setting = settingsMapping[index]
			if not setting[C_VALUES]:
				return

			text = self.getSettingOutput(setting)
			selection = self.view.sel()
			cursorIndex = self.view.full_line(selection[0]).begin()
			self.view.run_command(C.R_INSERT_TEXT_TBD,
				{"insertAt": cursorIndex, "text": text, "selectReversed": (-len(setting[C_VALUES][0]),0)})

	def getSettingOutput(self, setting):
		text = "> {0}\n".format(self.getSettingValues(setting))
		text += "§setting[{0}]={1}\n".format(setting[C_KEY], setting[C_VALUES][0])
		return text

	def getSettingValues(self, setting):
		return "Possible values: " + ", ".join(setting[C_VALUES])

	def getPanelInfo(self, setting):
		title = "setting={0}".format(setting[C_KEY])
		subTitle = self.getSettingValues(setting)
		return [title, subTitle]

	def run(self, edit):
		isDevEnv = Settings().get(C.S_IS_DEV_ENV, False)
		if (isDevEnv):
			print("#### Starting TextTBD ####")

		try:
			items = [self.getPanelInfo(entry) for entry in settingsMapping]
			self.view.window().show_quick_panel(items, self.insertSetting, False, -1)

		except TBDException as e:
			print("Error: " + e.value)
			if e.showError:
				sublime.error_message(e.value)
		except:
			e = sys.exc_info()[0]
			sublime.error_message("Unexpected Error, see log for more.\n" + str(e))
			raise e

		if (isDevEnv):
			print("#### Finished TextTBD ####")


class InsertTextTbdCommand(sublime_plugin.TextCommand):
	def run(self, edit, insertAt, text, selectReversed=None):
		isDevEnv = Settings().get(C.S_IS_DEV_ENV, False)
		if (isDevEnv):
			print("#### Starting InsertTextTBD ####")

		if insertAt and text:
			self.view.insert(edit, insertAt, text)
			if selectReversed:
				endOfLine = insertAt+len(text)-1
				newSelection = sublime.Region(endOfLine+selectReversed[0], endOfLine+selectReversed[1])
				selection = self.view.sel()
				selection.clear()
				selection.add(newSelection)


		if (isDevEnv):
			print("#### Finished InsertTextTBD ####")


class SfprintCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        self.view.insert(edit, self.view.size(), 'hello')

class SfCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        self.panel = self.view.window().create_output_panel('sf_st3_output')
        self.view.window().run_command('show_panel', { 'panel': 'output.sf_st3_output' })
        self.panel.run_command('sfprint');

        